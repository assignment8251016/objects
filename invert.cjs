function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    const new_obj = {}
    for(const key in obj){
        new_obj[obj[key]] = key
    }

    return new_obj;
}
module.exports = invert;