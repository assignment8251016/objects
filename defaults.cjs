function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    for( const key in obj){
        if(defaultProps[key] === obj[key]){
            for (const key in defaultProps){
                if( obj[key] === undefined){
                    obj[key] = defaultProps[key]
                }
            }
            return obj;
        }
    }
    return obj;
}

module.exports = defaults;